<?php namespace migrator;

class Migrator
{
	private $db_host, $db_database, $db_username, $db_password;
	private $mdb_host, $mdb_database, $mdb_username, $mdb_password;

	private $db, $mdb;
	private $migration_table = 'schema_migrations';
	private $migration_path;

	public function __construct($host, $database, $username, $password, $migration_path)
	{
		$this->db_host = $this->mdb_host = $host;
		$this->db_database = $this->mdb_database = $database;
		$this->db_username = $this->mdb_username = $username;
		$this->db_password = $this->mdb_password = $password;

		$this->db = $this->mdb = new \PDO("mysql:host=$host;dbname=$database", $username, $password);

		$this->migration_path = $migration_path;
	}

	public function setMigrationDatabase($host, $database, $username, $password)
	{
		$this->mdb_host = $host;
		$this->mdb_database = $database;
		$this->mdb_username = $username;
		$this->mdb_password = $password;

		$this->mdb = new \PDO("mysql:host=$host;dbname=$database", $username, $password);
	}

	public function setMigrationTable($table)
	{
		$this->migration_table = $table;
	}

	public function diff()
	{
		$this->check();
		$list = $this->getDiffList();

		return $list;
	}

	public function applydiff()
	{
		$this->check();
		$list = $this->getDiffList();

		$this->db->beginTransaction();

		try
		{
			foreach ($list as $file => $sql)
			{
				$valid = $this->db->exec($sql);

				if (!$valid)
				{
					throw new \Exception($sql);
				}
			}

			// TODO: test this for errors as well and roll back the migrations as well
			$stmt = $this->mdb->prepare("
				INSERT INTO {$this->migration_table}
				(version)
				VALUES
				(:version)
			");

			$stmt->bindParam('version', $file);
			$stmt->execute();

			$this->db->commit();
		}
		catch (\Exception $e)
		{
			$this->db->rollBack();

			throw new \Exception($e->getMessage());
		}

		return $list;
	}

	private function getDiffList()
	{
		$applied_result = $this->mdb->query("
			SELECT `version`
			FROM `{$this->migration_table}`
			ORDER BY `version` DESC
		")->fetchAll();

		$applied = [];

		foreach ($applied_result as $a)
		{
			$applied[] = $a['version'];
		}

		$file_list = $this->getFileList();

		$diff = array_diff($file_list, $applied);

		$full_diff = [];
		foreach ($diff as $d)
		{
			$full_diff[$d] = file_get_contents($this->migration_path . '/' . $d);
		}

		return $full_diff;
	}

	private function getFileList()
	{
		$files = [];
		
		if ($handle = opendir($this->migration_path))
		{
			// read the dir and check for files and sql in the name
			while (false !== ($file = readdir($handle)))
			{
				if (is_file($this->migration_path . '/' . $file) && preg_match("/(\d*)[_](.*)\.sql$/", $file))
				{
					$files[] = $file;
				}
			}

			closedir($handle);
		}
		
		return $files;
	}

	private function check()
	{

		$exists = $this->mdb->query('DESC schema_migrations');
		
		if ($exists === false)
		{
			$setup_migrations = <<<EOF
CREATE TABLE `{$this->migration_table}` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB;
EOF;
			
			$this->mdb->exec($setup_migrations);
		}
	}
}