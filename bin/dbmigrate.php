<?php

include __DIR__ . '/../vendor/autoload.php';

$options = getopt('', [
	
	// db setup
	'host::',
	'database::',
	'username::',
	'password::',

	// path to migrations
	'path::',

	// migrations db options
	'host::',
	'database::',
	'username::',
	'password::',
	'table::',

	// commands
	'diff',
	'applydiff',

	// options
	'verbose'
]);

$action = null;

if (!$action && isset($options['diff']))
{
	$action = 'diff';
}

if (!$action && isset($options['applydiff']))
{
	$action = 'applydiff';
}

$required_fields = ['host', 'database', 'username', 'password', 'path'];

$valid_command = true;
$error_message = '';
foreach ($required_fields as $r)
{
	if (!isset($options[$r]))
	{
		$error_message = 'Please provide a --' . $r;
		$valid_command = false;
	}
}

if (!$action)
{
	$error_message = 'Please provide a valid command (see below)';
	$valid_command = false;
}

$verbose = isset($options['verbose']);

if (!$valid_command)
{
	// default usage/error message

$str = <<<EOF
#########################################################################
########################### DI DB MIGRATOR ##############################
#########################################################################
#
# MESSAGE: $error_message
#
#########################################################################
#
# USAGE:
#
# Required Configuration:
#   --host (database host)
#   --database (database name)
#   --username (database user name)
#   --password (database password)
#   --path (valid path to migrations)
#
# Optional Configuration:
#   --mhost (database host where migrations table exists)
#   --mdatabase (database name where migrations table exists)
#   --musername (username of database where migrations table exists)
#   --mpassword (password of database where migrations table exists)
#   --mtable (table name for migrations -- defaults to schema_migrations)
#
# Commands:
#   --diff (shows a diff of all migrations not run)
#   --applydiff (applies the diff of migrations to be run)
#
# Misc Options:
#   --verbose (show a more verbose result)
#
#########################################################################

EOF;

	die($str);
}

$migrator = new \migrator\Migrator(
	$options['host'],
	$options['database'],
	$options['username'],
	$options['password'],
	$options['path']
);

// see if migrations databse is being set

$migration_fields = [
	'mhost',
	'mdatabase',
	'musername',
	'mpassword'
];

$custom_migration_setup = false;
foreach ($migration_fields as $f)
{
	if (isset($options[$f]) && $options[$f])
	{
		$custom_migration_setup = true;
	}
}

if ($custom_migration_setup)
{
	$mhost = isset($options['mhost']) && $options['mhost'] ? $options['mhost'] : $options['host'];
	$mdatabase = isset($options['mdatabase ']) && $options['mdatabase '] ? $options['mdatabase '] : $options['database'];
	$musername = isset($options['musername']) && $options['musername'] ? $options['musername'] : $options['username'];
	$mpassword = isset($options['mpassword']) && $options['mpassword'] ? $options['mpassword'] : $options['password'];

	$migrator->setMigrationDatabase($mhost, $mdatabase, $musername, $mpassword);
}

if (isset($options['mtable']) && $options['mtable'])
{
	$migrator->setMigrationTable($options['mtable']);
}

switch ($action)
{
	case 'diff':

		try
		{
			$result = $verbose ? $migrator->diff() : array_keys($migrator->diff());
			print_r($result);
		}
		catch (\Exception $e)
		{
			echo 'Error doing diff: ' . $e->getMessage();
		}

	break;

	case 'applydiff':
		
		try
		{
			$result = $verbose ? $migrator->applydiff() : array_keys($migrator->applydiff());
			print_r($result);
		}
		catch (\Exception $e)
		{
			echo 'Error running migrations: ' . $e->getMessage();
		}
		
	break;
}
